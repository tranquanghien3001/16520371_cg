#include "Parapol.h"
#include <iostream>
using namespace std;

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	BresenhamDrawParapolPositive_1(xc, yc, A, ren);
	BresenhamDrawParapolPositive_2(xc, yc, A, ren);
}

void BresenhamDrawParapolPositive_1(int xc, int yc, int A, SDL_Renderer *ren)
{
	int *w, *h;
	int w0, h0;
	w0 = 0;
	h0 = 0;
	w = &w0;
	h = &h0;
	SDL_GetRendererOutputSize(ren, w, h);
	
	int p = A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0)
		{
			p += 2 * A - 1 - 2 * x;
			y++;
		}
		else
		{
			p += -2 * x - 1;
		}
		x++;
		if ((x > *w) || (y > *h)) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolPositive_2(int xc, int yc, int A, SDL_Renderer *ren)
{
	int *w, *h;
	int w0, h0;
	w0 = 0;
	h0 = 0;
	w = &w0;
	h = &h0;
	int x = A + 1;
	int y = A / 2;
	SDL_GetRendererOutputSize(ren, w, h);
	int p = -2 * x - 1;
	while (y < *h)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * (x + 1);
			x++;
		}
		y++;
		if ((x > *w) || (y > *h)) break;
		else Draw2Points(xc, yc, x, y, ren);
	}

}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	BresenhamDrawParapolNegative_1(xc, yc, A, ren);
	BresenhamDrawParapolNegative_2(xc, yc, A, ren);
}

void BresenhamDrawParapolNegative_1(int xc, int yc, int A, SDL_Renderer *ren)
{
	int *w, *h;
	int w0, h0;
	w0 = 0;
	h0 = 0;
	w = &w0;
	h = &h0;
	SDL_GetRendererOutputSize(ren, w, h);

	int p = -A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0)
		{
			p +=  1 + 2 * x;
		}
		else
		{
			p += -2 * A + 2 * x + 1;
			y--;
		}
		x++;
		if ((x < -*w) || (y < -*h)) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative_2(int xc, int yc, int A, SDL_Renderer *ren)
{
	int *w, *h;
	int w0, h0;
	w0 = 0;
	h0 = 0;
	w = &w0;
	h = &h0;
	int x = A + 1;
	int y = -A / 2;
	SDL_GetRendererOutputSize(ren, w, h);
	int p = 2 * x + 1;
	while (y < *h)
	{
		if (p <= 0)
		{
			p += -4 * A + 4 * (x + 1);
			x++;
		}
		else
		{
			p += -4 * A;
		}
		y--;
		if ((x < -*w) || (y < -*h)) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
}