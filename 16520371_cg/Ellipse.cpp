#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2 = a * a;
	int b2 = b * b;
	float p = 2 * ((float)b2 / a2) - (2 * b) + 1;;

	int x, y;
	x = 0;
	y = b;



	//Area 1
	while (((float)b2 / a2)*x <= y)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * ((float)b2 / a2)*(2 * x + 3);
		}
		else {
			p += -4 * y + 2 * ((float)b2 / a2)*(2 * x + 3);
			y--;
		}
		x++;
	}
	//Area 2
	y = 0;
	x = a;
	p = 2 * ((float)a2 / b2) - 2 * a + 1;
	while (((float)a2 / b2) * y <= x)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * ((float)a2 / b2)*(2 * y + 3);
		}
		else
		{
			p += -4 * x + 2 * ((float)a2 / b2)*(2 * y + 3);
			x--;
		}
		y++;
	}

}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	//Area 1
	int a2 = a * a;
	int b2 = b * b;
	int x = 0;
	int y = b;
	float p = b2 - a2 * b + a2 * 0.25;

	Draw4Points(xc, yc, x, y, ren);
	while ((x * x) <= (a2 * a2) / (a2 + b2))
	{
		if (p <= 0)
		{
			p += b2 * (2 * x + 3);
		}
		else
		{
			p += b2 * (2 * x + 3) + 2 * a2 * (-y + 1);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}


	//Area 2
	x = a;
	y = 0;
	p = -a * b2 + a2 + 0.25 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while ((x * x) >= (a2 * a2) / (a2 + b2))
	{
		if (p <= 0)
		{
			p += a2 * (2 * y + 3);
		}
		else
		{
			p += a2 * (2 * y + 3) + 2 * b2 * (-x + 1);
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
	
}



