#include "DrawPolygon.h"
#include <iostream>
#define PI 3.1415926535897
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 3;
	}
	for (int i = 0; i < 3; i++)
		Bresenham_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = PI / 4;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 4;
	}
	for (int i = 0; i < 4; i++)
		Bresenham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);

	
	
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	
	for (int i = 0; i < 5; i++)
		Bresenham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = 4 * PI / 6;
	for (int i = 0; i < 6; i++) 
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 6;
	}

	for (int i = 0; i < 6; i++)
		Bresenham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		int k = (i + 2) % 5;
		Bresenham_Line(x[i], y[i], x[k], y[k], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], x1[5], y1[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	phi = 7 * PI / 10;
	int r = R*sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x1[i] = xc + int(r*cos(phi) + 0.5);
		y1[i] = yc - int(r*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		int k = (i + 1) % 5;
		Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
		Bresenham_Line(x1[i], y1[i], x[k], y[k], ren);
	}

}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8], x1[8], y1[8];
	float phi = PI / 2;
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 8;
	}
	phi = 5 * PI / 8;
	int r = R*sin(PI / 8) / sin(3 * PI / 4);
	for (int i = 0; i < 8; i++)
	{
		x1[i] = xc + int(r*cos(phi) + 0.5);
		y1[i] = yc - int(r*sin(phi) + 0.5);
		phi += 2 * PI / 8;
	}
	for (int i = 0; i < 8; i++)
	{
		int k = (i + 1) % 8;
		Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
		Bresenham_Line(x1[i], y1[i], x[k], y[k], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	if (R < 1) return;
	else
	{
		int x[5], y[5], x1[5], y1[5];
		for (int i = 0; i < 5; i++)
		{
			x[i] = xc + int(R*cos(startAngle) + 0.5);
			y[i] = yc - int(R*sin(startAngle) + 0.5);
			startAngle += 2 * PI / 5;
		}
		startAngle = startAngle + PI / 5;
		int r = (int)(R*sin(PI / 10) / sin(7 * PI / 10) + 0.5);
		for (int i = 0; i < 5; i++)
		{
			x1[i] = xc + int(r*cos(startAngle) + 0.5);
			y1[i] = yc - int(r*sin(startAngle) + 0.5);
			startAngle += 2 * PI / 5;
		}
		for (int i = 0; i < 5; i++)
		{
			int k = (i + 1) % 5;
			Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
			Bresenham_Line(x1[i], y1[i], x[k], y[k], ren);
		}
		startAngle += PI - PI / 5;
		DrawStarAngle(xc, yc, r, startAngle, ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	DrawStarAngle(xc, yc, r, PI/2, ren);
}
