#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Circle.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE


	SDL_Color fillColor;
	fillColor.r = 255;
	fillColor.g = 0;
	fillColor.b = 255;
	fillColor.a = 255;

	SDL_Color BoundaryColor;
	BoundaryColor.r = 255;
	BoundaryColor.g = 100;
	BoundaryColor.b = 100;
	BoundaryColor.a = 255;

	Vector2D v1(100, 200);
	Vector2D v2(200, 100);
	Vector2D v3(300, 300);

	Vector2D a(100, 100);
	Vector2D b(200, 700);
	Vector2D c(500, 650);
	Vector2D d(600, 300);


	SDL_SetRenderDrawColor(ren, 255, 0, 255, 255);
	DrawCurve3(ren, a, b, c, d);
	//DrawCurve2(ren, a, b, c);


	//TriangleFill(v1, v2, v3, ren, fillColor);
	//CircleFill(200, 200, 100, ren, fillColor);
	//RectangleFill(v3, v1, ren, fillColor);
	//FillIntersectionRectangleCircle(v3, v1, 20, 400, 300, ren, fillColor);
	//FillIntersectionEllipseCircle(250, 400, 300, 200, 400, 400, 200, ren, fillColor);
	//FillIntersectionTwoCircles(300, 300, 200, 400, 200, 300, ren, fillColor);

	//==========Boundary Fill==============

	/*SDL_SetRenderDrawColor(ren, 255, 100, 100, 255);
	BresenhamDrawCircle(200, 200, 4, ren);
	Vector2D StartPoint(200, 200);
	Uint32 pixel_format = SDL_GetWindowPixelFormat(win);
	BoundaryFill4(win, StartPoint , pixel_format,ren, fillColor, BoundaryColor);*/




    SDL_RenderPresent(ren);
    //Take a quick break after all that hard work
    //Quit if happen QUIT event
	

	bool running = true;

	int dx, dy;
	float Da = 20, Db = 20, Dc = 20, Dd = 20;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
			
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				dx = event.button.x;
				dy = event.button.y;
				Da = sqrt((dx - a.x)*(dx - a.x) + (dy - a.y)*(dy - a.y));
				Db = sqrt((dx - b.x)*(dx - b.x) + (dy - b.y)*(dy - b.y));
				Dc = sqrt((dx - c.x)*(dx - c.x) + (dy - c.y)*(dy - c.y));
				Dd = sqrt((dx - d.x)*(dx - d.x) + (dy - d.y)*(dy - d.y));
			}


			if (event.type == SDL_MOUSEMOTION)
			{
				if (Da <= 10) {
				    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					a.x = event.button.x;
					a.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					DrawCurve3(ren, a, b, c, d);
					//DrawCurve2(ren, a, b, c);
					SDL_RenderPresent(ren);
				}
				else if (Db <= 10) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					b.x = event.button.x;
					b.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					DrawCurve3(ren, a, b, c, d);
					//DrawCurve2(ren, a, b, c);
					SDL_RenderPresent(ren);
				}
				else if (Dc <= 10) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					c.x = event.button.x;
					c.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					DrawCurve3(ren, a, b, c, d);
					//DrawCurve2(ren, a, b, c);
					SDL_RenderPresent(ren);
				}
				else if (Dd <= 10) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					d.x = event.button.x;
					d.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					DrawCurve3(ren, a, b, c, d);
					//DrawCurve2(ren, a, b, c);
					SDL_RenderPresent(ren);
				}
			}


			if (event.type == SDL_MOUSEBUTTONUP)
			{
				Da = 20;
				Db = 20;
				Dc = 20;
				Dd = 20;
			}
		}
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
