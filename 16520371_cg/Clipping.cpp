#include "Clipping.h"
#include <iostream>]
using namespace std;

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int Case = CheckCase(c1, c2);
	while(Case == 3)
	{
		if (c1 != 0)
			ClippingCohenSutherland(r, P1, P2);
		else
			ClippingCohenSutherland(r, P2, P1);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		Case = CheckCase(c1, c2);
	}
	if (Case == 2)
		return 0;
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int c1 = Encode(r, P1);
	int Dx = P2.x - P1.x;
	int Dy = P2.y - P1.y;
	float m;
	if (Dx != 0)
		m = (float)Dy / (float)Dx;
	else return;
	if (c1 & LEFT != 0)
	{
		P1.y = P1.y + (r.Left - P1.x) * m;
		P1.x = r.Left;
		return;
	}
	if (c1 & RIGHT != 0)
	{
		P1.y = P1.y + (r.Right - P1.x) * m;
		P1.x = r.Right;
		return;
	}
	if (c1 & TOP != 0)
	{
		P1.x = P1.x + (r.Top - P1.y) / m;
		P1.y = r.Top;
		return;
	}
	if (c1 & BOTTOM != 0)
	{
		P1.x = P1.x + (r.Bottom - P1.y) / m;
		P1.y = r.Bottom;
		return;
	}
	return;
}




int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}


int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1, t2;
	int Dx, Dy, x1, y1, x2, y2, xmin, ymin, xmax, ymax;
	t1 = 0;
	t2 = 1;
	x1 = P1.x;
	y1 = P1.y;
	x2 = P2.x;
	y2 = P2.y;
	Dx = x2 - x1;
	Dy = y2 - y1;
	xmin = r.Left;
	xmax = r.Right;
	ymin = r.Top;
	ymax = r.Bottom;
	if (SolveNonLinearEquation(-Dx, x1 - xmin, t1, t2))
	{
		if (SolveNonLinearEquation(Dx, xmax - x1, t1, t2))
		{
			if (SolveNonLinearEquation(-Dy, y1 - ymin, t1, t2))
			{
				if (SolveNonLinearEquation(Dy, ymax - y1, t1, t2))
				{
					Q1.x = x1 + t1*Dx;
					Q1.y = y1 + t1*Dy;
					Q2.x = x1 + t2*Dx;
					Q2.y = y1 + t2*Dy;
					cout << Q1.x << " " << Q1.y << " " << Q2.x << " " << Q2.y;
					return 1;
				}
			}
		}
	}

	return 0;
}
